<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

// If you don't want to setup permissions the proper way, just uncomment the following PHP line
// read http://symfony.com/doc/current/book/installation.html#configuration-and-setup for more information
//umask(0000);

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1'))
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}

// Read bootstrap directory from environment variable
$bootstrapDir = __DIR__ . '/../app/';
$bootstrapDirEnvVarName = 'SYMFONY_BOOTSTRAP_DIR';
if (getenv($bootstrapDirEnvVarName)) {
    $bootstrapDir = getenv($bootstrapDirEnvVarName);
}
$loader = require_once $bootstrapDir . '/bootstrap.php.cache';

// We don't want to use the cached/compiled framework files when step-debugging
// so we can set breakpoints in framework code
$enableCaching = !extension_loaded('xdebug') || (!isset($_REQUEST['XDEBUG_SESSION_START']) && !isset($_COOKIE['XDEBUG_SESSION']) && ini_get('xdebug.remote_autostart') == false);

if ($enableCaching) {
    $loader = require_once $bootstrapDir . '/bootstrap.php.cache';
} else {
    $vendorDirEnvVarName = 'COMPOSER_VENDOR_DIR';
    $vendorDir = __DIR__ . '/../vendor';
    if (getenv($vendorDirEnvVarName)) {
        $vendorDir = getenv($bootstrapDirEnvVarName);
    }

    require_once $vendorDir . '/autoload.php';
}

Debug::enable();

require_once __DIR__.'/../app/AppKernel.php';

$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
