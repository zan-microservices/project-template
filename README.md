Zan Microservices - Project Template
========================================

This is an empty project to quickly start on implementing a microservice.

It sets up the following environment:

 * Local SQLite database
 
## Installation

    composer create-project zan/microservices-project-template new-directory -s dev


