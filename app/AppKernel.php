<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }

    /**
     * Overridden to read from SYMFONY_CACHE_DIR environment variable
     *
     * @return string
     */
    public function getCacheDir()
    {
        $dirEnvironmentVariableName = 'SYMFONY_CACHE_DIR';
        if (getenv($dirEnvironmentVariableName)) {
            return getenv($dirEnvironmentVariableName) . '/' . $this->environment;
        }
        else {
            return $this->rootDir . '/cache/' . $this->environment;
        }
    }

    /**
     * Overridden to read from SYMFONY_LOG_DIR environment variable
     *
     * @return string
     */
    public function getLogDir()
    {
        $dirEnvironmentVariableName = 'SYMFONY_LOG_DIR';
        if (getenv($dirEnvironmentVariableName)) {
            return getenv($dirEnvironmentVariableName);
        }
        else {
            return $this->rootDir . '/logs';
        }
    }

    /**
     * Gets the environment parameters.
     *
     * Only the parameters starting with "SYMFONY__" are considered.
     *
     * Overridden to handle setting parameters that must be a boolean
     *
     * @return array An array of parameters
     */
    protected function getEnvParameters()
    {
        $parameters = array();
        foreach ($_SERVER as $key => $value) {
            if (0 === strpos($key, 'SYMFONY__')) {
                /*
                 * Special case: when reading a parameter from the environment,
                 * there is no way to specify that one is a boolean so you get
                 * errors like:
                 *
                 * [Symfony\Component\Config\Definition\Exception\InvalidTypeException]
                 * Invalid type for path "swiftmailer.mailers.default.disable_delivery". Expected boolean, but got string.
                 *
                 * To fix this, we support a crude kind of type hinting:
                 *
                 * export SYMFONY__mailer_disable_delivery=false[type:boolean]
                 *
                 * @dockerfix
                 * todo: better way?
                 */
                // Check if the variable ends with our special type hint syntax
                $search = '[type:boolean]';
                if (strrpos($value, $search) === strlen($value) - strlen($search)) {
                    $processedValue = substr($value, 0, strlen($value) - strlen($search));
                    if ('false' === $processedValue || !$processedValue) {
                        $value = false;
                    }
                    else {
                        $value = true;
                    }
                }

                $parameters[strtolower(str_replace('__', '.', substr($key, 9)))] = $value;
            }
        }

        return $parameters;
    }
}
